require('dotenv').config()
//console.log(process.env)
const http = require('http');
const app = require('./app');
const PORT = process.env.PORT || 6000;

const server = http.createServer(app)

server.listen(PORT, ()=>{
    console.log(`Server is listening on PORT ${PORT}`)
})

