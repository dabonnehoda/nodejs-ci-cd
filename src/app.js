const express = require('express');
const fileUpload = require('express-fileupload')
const cors = require('cors');
const morgan = require('morgan')
const path = require('path');


const app = express();
app.use(express.urlencoded({extended: true}))
app.use(fileUpload())

app.use(cors({
        origin: '*'
    }
));
app.use(morgan('combined'))
app.use(express.json());


module.exports = app;
